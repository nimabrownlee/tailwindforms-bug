const Navbar = () => {
  return (
    <div>
      <div className="flex items-center max-w-7xl mx-auto px-8">
        <div className="ml-auto">
          <button className="mr-6 py-2.5 px-4 rounded-md bg-indigo-500">
            Sign in
          </button>
          <input className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
        </div>
      </div>
    </div>
  )
}

export default Navbar
